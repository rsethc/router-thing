#pragma once 

#include <map>
#include <string>
#include <stdexcept>

class Request
{
public:
    std::string path;
    Request (std::string path);
    virtual ~Request ();
    virtual void Respond (std::string status, std::string body) = 0;
};

class Params
{
    std::map<std::string, int> values;
public:
    void Set (std::string key, int value);
    bool TryGet (std::string key, int& value);
};

std::string NextSegment (std::string& remaining);

typedef void (ResponderFunc) (Request* request_obj, Params& params_obj);

class IntegerSegment;
class RouterNode
{
    std::map<std::string, RouterNode*> literal_matches;
    IntegerSegment* integer_match = NULL;
    ResponderFunc* direct_match = NULL;

public:
    ResponderFunc* Explore (std::string remaining, Params& params);

    friend class IntegerSegment;
    friend class LiteralSegment;
};

class Root : public RouterNode
{
public:
    void Serve (Request* request);
};

class LiteralSegment : public RouterNode
{
public:
    LiteralSegment (RouterNode* parent, std::string literal, ResponderFunc* direct_match = NULL);
};

class IntegerSegment : public RouterNode
{
public:
    std::string param_name;
    IntegerSegment (RouterNode* parent, std::string param_name, ResponderFunc* direct_match = NULL);
};
