#pragma once

#include "router.h"

struct TheAPI_t
{
    Root root;
    LiteralSegment api { &root, "api" };
};

void ServeLogin (Request* request, Params& params);

struct V1API_t
{
    LiteralSegment v1;
    V1API_t (RouterNode* api_root) : v1 { api_root, "v1" } {}
    LiteralSegment user { &v1, "user" };
    IntegerSegment userid { &user, "userid" };
    LiteralSegment login { &userid, "login", ServeLogin };
};
