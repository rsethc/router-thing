#pragma once

#include "router.h"

class FakeRequest : public Request
{
public:
    FakeRequest (std::string path);
    ~FakeRequest () override;
    void Respond (std::string status, std::string body) override;
};
