#include "router.h"

using namespace std;

Request::Request (string path) : path(path) {}
Request::~Request () {}

void Params::Set (string key, int value)
{
    values[key] = value;
}
bool Params::TryGet (string key, int& value)
{
    try
    {
        value = values.at(key);
        return true;
    }
    catch (out_of_range)
    {
        return false;
    }
}

string NextSegment (string& remaining)
{
    string::size_type next_slash = remaining.find_first_of('/');
    string out;
    if (next_slash == string::npos)
    {
        out = remaining;
        remaining = "";
    }
    else
    {
        out = remaining.substr(0, next_slash);
        remaining = remaining.substr(next_slash + 1);
    }
    return out;
}

ResponderFunc* RouterNode::Explore (string remaining, Params& params)
{
    if (remaining.length() == 0)
    {
        return direct_match; // Whether or not it's NULL.
    }

    string lower = NextSegment(remaining);

    try
    {
        return literal_matches.at(lower)->Explore(remaining, params);
    }
    catch (out_of_range) {}

    if (!integer_match) return NULL;

    try
    {
        params.Set(integer_match->param_name, stoi(lower));
        return integer_match->Explore(remaining, params);
    }
    catch (out_of_range)     {}
    catch (invalid_argument) {}

    return NULL;
}

void RespondFailure (Request* request, Params& params)
{
    request->Respond("404 Not Found", "go away");
}

void Root::Serve (Request* request)
{
    Params params;
    ResponderFunc* respond = Explore(request->path, params);
    if (!respond) respond = RespondFailure;
    respond(request,params);
}

LiteralSegment::LiteralSegment (RouterNode* parent, string literal, ResponderFunc* direct_match)
{
    this->direct_match = direct_match;
    parent->literal_matches[literal] = this;
}

IntegerSegment::IntegerSegment (RouterNode* parent, string param_name, ResponderFunc* direct_match)
    : param_name(param_name)
{
    this->direct_match = direct_match;
    parent->integer_match = this;
}
