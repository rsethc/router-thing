#include "sample-api.h"

using namespace std;

void ServeLogin (Request* request, Params& params)
{
    int userid;
    if (!params.TryGet("userid", userid))
    {
        // This should be impossible.
        request->Respond("500 Internal Server Error", "Aurora Borealis?");
        return;
    }

    request->Respond("200 OK", "hello, user number " + to_string(userid) + ", we've been expecting you...");
}
