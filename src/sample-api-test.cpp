#include "sample-api.h"
#include "sample-api-test.h"

#include <iostream>

using namespace std;

TheAPI_t TheAPI_Singleton;

V1API_t V1API_singleton { &TheAPI_Singleton.api };

FakeRequest::FakeRequest (string path)
    : Request(path)
{
    cout << "Request for " << path << endl;
}
FakeRequest::~FakeRequest ()
{
    cout << "Processed request for " << path << endl;
}
void FakeRequest::Respond (string status, string body)
{
    cout << "Response for " << path << endl;
    cout << status << endl;
    cout << body << endl;
}

Request* GetNextRequest ()
{
    static int times = 5;
    if (times <= 0) return NULL;
    times--;

    if (!times) return new FakeRequest { "gibberish/whatever" };

    static int id = -1;
    return new FakeRequest { "api/v1/user/" + to_string(id++) + "/login" };
}

int main ()
{
    while (Request* req = GetNextRequest())
    {
        TheAPI_Singleton.root.Serve(req);
        delete req;
    }
    return 0;
}
